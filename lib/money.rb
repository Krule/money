require "configurations"
require 'forwardable'

require_relative "money/version"
require_relative "money/money"
require_relative "money/exchange"
require_relative "money/currency"

module Money
  extend SingleForwardable
  def_delegators "Money::Money", "new"
  def_delegators "Money::Exchange", "currencies"

  include Configurations

  configurable Integer, :base_precision
  configurable String, :default_currency

  configuration_defaults do |c|
    c.base_precision = 2
    c.default_currency = "EUR"
  end

  class << self
    def conversion_rates(from_name, currency_rates)
      base_currency = Exchange.add_currency(Currency.instance(from_name))

      currency_rates.each do |name, rate|
        currency = Exchange.add_currency(Currency.instance(name))
        Exchange.add_exchange_rate(base_currency, currency, BigDecimal(rate.to_s))
      end

      currency_rates.keys.combination(2).each do |left, right|
        left_currency = Currency.instance(left)
        left_rate = left_currency.conversion_rate(from_name)

        right_currency = Currency.instance(right)
        right_rate = right_currency.conversion_rate(from_name)

        rate = BigDecimal(left_rate.to_s) / BigDecimal(right_rate.to_s)

        Exchange.add_exchange_rate(left_currency, right_currency, rate)
      end
    end
  end
end

class Numeric
  def to_money(currency = Money.configuration.default_currency)
    Money.new(BigDecimal(self.to_s), currency)
  end
end
