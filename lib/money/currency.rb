require "bigdecimal"
require "singleton"

require_relative "errors"

module Money
  class Currency
    include Singleton

    attr_reader :name,
                :conversion_rates

    def initialize(name)
      @name = name
      @conversion_rates = { key(name) => BigDecimal(1) }
    end

    def self.instance(name)
      Exchange.currency(name) || new(name)
    end

    def add_conversion_rate(currency, conversion_rate)
      @conversion_rates[key(currency.name)] = conversion_rate
    end

    def conversion_rate(name)
      @conversion_rates[key(name)].to_f
    end

    def to_s
      name
    end

    alias_method :inspect, :to_s

    private

    def key(to)
      "#{name}->#{to}"
    end
  end
end
