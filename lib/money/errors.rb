module Money
  module Errors
    class NotNumber < StandardError
      def initialize(object, message = "`#{object}`")
        super(message)
      end
    end

    class NotMoney < StandardError
      def initialize(object, message = "`#{object}`")
        super(message)
      end
    end

    class UndefinedCurrency < StandardError
      def initialize(object, message = "`#{object}`")
        super(message)
      end
    end
  end
end
