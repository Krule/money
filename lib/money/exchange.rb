require 'bigdecimal'

module Money
  module Exchange
    @@currencies = {}

    class << self
      def add_currency(currency)
        @@currencies[currency.name] ||= currency
        currency
      end

      def add_exchange_rate(left, right, conversion_rate)
        @@currencies[left.name].add_conversion_rate(right, conversion_rate)
        @@currencies[right.name].add_conversion_rate(left, 1 / conversion_rate)
      end

      def currency(name)
        @@currencies[name]
      end

      def currencies
        @@currencies.keys
      end
    end
  end
end
