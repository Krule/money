require "bigdecimal"

require_relative "currency"
require_relative "errors"

module Money
  class Money
    include Comparable

    attr_reader :cents,
                :currency

    def initialize(amount, currency = ::Money.configuration.default_currency)
      fail Errors::NotNumber, amount unless amount.is_a?(Numeric)

      @currency = if currency.respond_to?(:conversion_rates)
                    currency
                  else
                    Currency.instance(currency.to_s)
                  end
      @amount = BigDecimal(amount.to_s)
      @cents = (@amount / 100).truncate(4)
    end

    def amount
      @amount.frac == 0 ? @amount.to_i : @amount.to_f
    end

    def <=>(other)
      cents <=> conditional_convert(other.to_money).cents
    end

    def +(other)
      self.class.new(@amount + converted_money(other).amount, currency)
    end

    def -(other)
      self.class.new(@amount - converted_money(other).amount, currency)
    end

    def /(other)
      self.class.new(@amount / other, currency)
    end

    def *(other)
      self.class.new(@amount * other, currency)
    end

    # Argument present in order to satisfy protocol defined for Numeric
    def to_money(_ = nil)
      self
    end

    def convert_to(name)
      fail Errors::UndefinedCurrency, name unless Exchange.currency(name)
      self.class.new((@amount * rate(name)), name)
    end

    def to_s
      "#{formated} #{currency}"
    end

    alias_method :inspect, :to_s

    private

    def converted_money(other)
      conditional_convert(other.to_money(currency.name))
    end

    def conditional_convert(other)
      return other if currency.name == other.currency.name
      other.convert_to(currency.name)
    end

    def formated
      sprintf("%.#{::Money.configuration.base_precision}f", @amount)
    end

    def rate(name)
      Currency.instance(currency.name).conversion_rate(name)
    end
  end
end
