require "spec_helper"

describe "Money::Currency" do
  context "signleton class" do
    subject { Money::Currency }
    it { is_expected.not_to respond_to(:new) }
    it { is_expected.to respond_to(:instance) }
  end

  context "class instance" do
    subject { Money::Currency.instance("EUR") }
    it { is_expected.to respond_to(:add_conversion_rate, :conversion_rate) }
  end

  describe "edges" do
    let(:eur) { Money::Currency.instance("EUR") }
    let(:usd) { Money::Currency.instance("USD") }
    let(:rate) { 1.1 }
    before { eur.add_conversion_rate(usd, rate) }
    subject { eur.conversion_rate("USD") }

    it { is_expected.to eq(rate) }
  end
end
