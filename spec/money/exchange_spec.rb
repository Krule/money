require "spec_helper"

describe Money::Exchange do
  context 'Module' do
    subject { Money::Exchange }
    it do
      is_expected.to respond_to(
        :add_currency,
        :add_exchange_rate,
        :currency,
        :currencies
      )
    end
  end

  context "verticles" do
    let(:eur) { Money::Currency.instance("EUR") }
    let(:usd) { Money::Currency.instance("USD") }

    before do
      Money::Exchange.add_currency(usd)
      Money::Exchange.add_currency(eur)
    end

    context 'add verticle' do
      subject{ Money::Exchange.add_currency(eur) }
      it { is_expected.to be(eur) }
      it { is_expected.to be_kind_of(Money::Currency) }
    end

    context 'store' do
      subject { Money::Exchange.currencies }

      it { is_expected.to include("USD") }
      it { is_expected.to include("EUR") }
    end

    context "verticle" do
      subject { Money::Exchange.currency("EUR") }
      it { is_expected.to be(eur) }
      it { is_expected.to be_kind_of(Money::Currency) }
    end
  end
end
