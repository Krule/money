require "spec_helper"

describe Money::Money do
  describe "class" do
    subject { Money::Money }
  end

  describe "initialize" do
    subject { Money.new(amount, currency) }
    let(:amount) { 100 }
    let(:currency) { nil }

    it { is_expected.to be_kind_of(Money::Money) }
    it { is_expected.to respond_to(:amount, :currency, :inspect, :convert_to) }

    context "bad argument" do
      it do
        expect(-> { Money.new("foo") }).to raise_error(Money::Errors::NotNumber)
      end
    end
  end

  describe "inspect" do
    subject { Money.new(amount, "EUR").inspect }

    context "one decimal point" do
      let(:amount) { 12.5 }
      it { is_expected.to eq("12.50 EUR") }
    end

    context "two decimal points" do
      let(:amount) { 15.45 }
      it { is_expected.to eq("15.45 EUR") }
    end

    context "more decimal places" do
      let(:amount) { 15.45724342 }
      it { is_expected.to eq("15.46 EUR") }
    end
  end

  describe "conversions" do
    let(:rates) { { 'USD' => 1.0837, 'Bitcoin' => 0.0028 } }
    let(:name) { "EUR" }
    before { Money.conversion_rates(name, rates) }

    context "valid currency" do
      subject { Money.new(10, "EUR").convert_to("USD") }
      it { is_expected.to eq(Money.new(10.8370, "USD")) }
    end

    context "invalid currency" do
      subject { -> { Money.new(10, "EUR").convert_to("BAM") } }
      it { is_expected.to raise_error(Money::Errors::UndefinedCurrency) }
    end

    context "over a third currency" do
      subject { Money.new(10, "USD").convert_to("Bitcoin") }
      it { is_expected.to eq(Money.new(0.0250, "Bitcoin")) }
    end

    describe "comparison" do
      subject { a <=> b }

      context "a < b" do
        let(:a) { Money.new(15, "EUR") }
        let(:b) { Money.new(30, "USD") }
        it { is_expected.to be -1 }
      end

      context "a == b" do
        let(:a) { Money.new(15, "EUR") }
        let(:b) { Money.new(16.26, "USD") }
        it { is_expected.to be 0 }
      end

      context "a > b" do
        let(:a) { Money.new(15, "EUR") }
        let(:b) { Money.new(5, "EUR") }
        it { is_expected.to be 1 }
      end
    end

    describe "calculus" do
      context "addition" do
        subject { a + b }

        context "with same currency" do
          let(:a) { Money.new(15, "EUR") }
          let(:b) { a }
          it { is_expected.to eq(Money.new(30, "EUR")) }
        end

        context "with different currency" do
          let(:a) { Money.new(10, "EUR") }
          let(:b) { Money.new(10, "USD") }
          let(:result) { 10 + BigDecimal("#{1 / 1.0837}").truncate(4) * 10 }
          it { is_expected.to eq(Money.new(result, "EUR")) }
        end

        context "with a number" do
          let(:a) { Money.new(10, "USD") }
          let(:b) { 5 }
          it { is_expected.to eq(Money.new(15, "USD")) }
        end
      end

      context "substraction" do
        subject { a - b }

        context "with same currency" do
          let(:a) { Money.new(15, "EUR") }
          let(:b) { Money.new(5, "EUR") }
          it { is_expected.to eq(Money.new(10, "EUR")) }
        end

        context "with different currency" do
          let(:a) { Money.new(10, "EUR") }
          let(:b) { Money.new(10, "USD") }
          let(:result) { 10 - BigDecimal("#{1 / 1.0837}").truncate(4) * 10 }
          it { is_expected.to eq(Money.new(result, "EUR")) }
        end

        context "with a number" do
          let(:a) { Money.new(10, "USD") }
          let(:b) { 5.5 }
          it { is_expected.to eq(Money.new(4.5, "USD")) }
        end
      end

      context "multiplication" do
        subject { a * b }

        let(:a) { Money.new(15, "EUR") }
        let(:b) { 2 }
        it { is_expected.to eq(Money.new(30, "EUR")) }
      end

      context "division" do
        subject { a / b }

        let(:a) { Money.new(15, "EUR") }
        let(:b) { 2 }
        it { is_expected.to eq(Money.new(7.5, "EUR")) }
      end
    end
  end
end
