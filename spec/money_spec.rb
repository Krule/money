require "spec_helper"

describe Money do
  context "version" do
    subject { Money::VERSION }

    it { is_expected.not_to be nil }
  end

  context "forwarding new" do
    subject { Money.new(0) }

    it { is_expected.to be_kind_of(Money::Money) }
  end

  describe "module" do
    subject { Money }

    it { is_expected.to respond_to(:conversion_rates) }
    it { is_expected.to respond_to(:configure) }
    it { is_expected.to respond_to(:configuration) }
    it { expect { |b| subject.configure(&b) }.to yield_with_args }
  end

  describe "set_conversion_rates" do
    context 'adds base currency to conversion rates' do
      subject { Money.currencies }
      let(:name) { "EUR" }
      let(:rates) { { 'USD' => 1.11, 'Bitcoin' => 0.0047 } }
      before { Money.conversion_rates(name, rates) }

      it { is_expected.to include("EUR") }
      it { is_expected.to include("USD") }
      it { is_expected.to include("Bitcoin") }
    end
  end
end
