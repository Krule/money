require "spec_helper"

describe Numeric do
  context 'to_money' do
    subject { Numeric.new() }
    it { is_expected.to respond_to(:to_money) }
  end

  context "money instance" do
    subject { Numeric.new().to_money }
    it { is_expected.to eq(Money.new(0, "EUR")) }
  end
end
